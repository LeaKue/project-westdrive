using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Animator))]
///<summary>
/// simultates the movement of the animated Taxi Driver
///<</summary>
public class TaxiDriverIK : MonoBehaviour {

    protected Animator animator;

    public bool ikActive = false;
    public Transform rightHandObj = null;
    public Transform leftHandObj = null;
    //public GameObject avatarGazeTarget;
    [Range(0, 1)]
    public float righthandWeight = 1f;
    [Range(0, 1)]
    public float lefthandWeight = 1f;
    [Range(0, 1)]
    public float avatarGazeStrenght = 0.6f;
    //private Quaternion gazeTargetOriginalRotation;
    void Start()
    {
        
        animator = GetComponent<Animator>();
    }

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {
            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {
                if (rightHandObj != null)
                {
                    UpdateIkPositionAndRotation(AvatarIKGoal.RightHand, righthandWeight, rightHandObj);
                    
                }
                if (leftHandObj != null)
                {
                    UpdateIkPositionAndRotation(AvatarIKGoal.LeftHand, lefthandWeight, leftHandObj);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                //TODO: Why is this value overwritten?
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
            }
        }
    }

    private void UpdateIkPositionAndRotation(AvatarIKGoal hand, float handWeight, Transform handObject)
    {
        animator.SetIKPositionWeight(hand, handWeight);
        animator.SetIKRotationWeight(hand, handWeight);
        animator.SetIKPosition(hand, handObject.position);
        animator.SetIKRotation(hand, handObject.rotation);
    }
}
