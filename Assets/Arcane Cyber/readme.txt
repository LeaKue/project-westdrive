Hi,
Thank you for purchasing the Traffic Signs Set.

There are 146 traffic sign and several traffic sign post in this pack.
Every model comes with five different LOD:

[Triangle Traffic Signs]
Lod ID						Vertice Count			Triangle Count			Texture Resolution
LOD0						60						116						2048 x 2048
LOD1						36						68						1024 x 1024
LOD2						24						44						512 x 512
LOD3						18						32						256 x 256
LOD4						6						8						128 x 128

[Square/Rectangle/Rhombus Traffic Signs]
Lod ID						Vertice Count			Triangle Count			Texture Resolution
LOD0						64						124						2048 x 2048
LOD1						40						76						1024 x 1024
LOD2						24						44						512 x 512
LOD3						16						28						256 x 256
LOD4						8						12						128 x 128

[Octagon Traffic Sign]
Lod ID						Vertice Count			Triangle Count			Texture Resolution
LOD0						64						124						2048 x 2048
LOD1						32						60						1024 x 1024
LOD2						32						60						512 x 512
LOD3						16						28						256 x 256
LOD4						16						28						128 x 128

[Circle Traffic Signs]
Lod ID						Vertice Count			Triangle Count			Texture Resolution
LOD0						128						254						2048 x 2048
LOD1						64						124						1024 x 1024
LOD2						32						60						512 x 512
LOD3						24						44						256 x 256
LOD4						16						28						128 x 128

[TS1 Stands]
Lod ID						Vertice Count			Triangle Count			Texture Resolution
LOD0						96						188						2048 x 2048
LOD1						64						124						1024 x 1024
LOD2						32						64						512 x 512
LOD3						16						28						256 x 256
LOD4						8						12						128 x 128


For more information about Level of Detail (LOD), please visit the following site:
https://docs.unity3d.com/Manual/LevelOfDetail.html

Feel free to contact me at arcanecyber@gmail.com, if you have any questions or you need a custom traffic sign.